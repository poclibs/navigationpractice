package POS_Libs.HttpConnection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class is to init and setup basic http connection and configuration
 * Created by potingchiang on 2016-06-29.
 *
 * ----------------------------
 * POS server status code list:
 * ----------------------------
 * POST/Create:
 * ok -> 201
 * NG -> 422
 * ----------------------------
 * PUT/Update:
 * ok -> 200
 * NG -> 422
 * ----------------------------
 * DELETE:
 * ok -> 200
 * NG ->
 *----------------------------
 *
 */

public class BasicHttpConnection_Json {

    //declaration
    //url
    private String url;
    //http connection
    private HttpURLConnection conn;
    private DataOutputStream dataOutputStream;
    private BufferedReader bufferedReader;
    private String request_Method;
    //message
    String message;

    //constructor
    public BasicHttpConnection_Json(String url, String request_Method) {
        this.url = url;
        this.request_Method = request_Method;

        try {

            //init. url
            URL mUrl = new URL(url);
            //init. connection
            conn = (HttpURLConnection) mUrl.openConnection();
            //setup configuration
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            //setup request method
            conn.setRequestMethod(request_Method);
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Content-Type","text/plain");

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("===\n" + e.getMessage() + "\n===");
        }
    }

    //getter & setter
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getRequest_Method() {
        return request_Method;
    }
    public void setRequest_Method(String request_Method) {
        this.request_Method = request_Method;
    }
    public DataOutputStream getDataOutputStream() {

        try {

            //init data output stream
            dataOutputStream = new DataOutputStream(conn.getOutputStream());
        } catch (IOException e) {

            //error messages
            e.printStackTrace();
            message = e.getMessage();
            System.out.print("===\nerror:" + e.getMessage() + "\n==");
        }

        return dataOutputStream;
    }
    public BufferedReader getBufferedReader() {

        try {

            //init input stream within buffer reader
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        } catch (IOException e) {

            //error messages
            e.printStackTrace();
            message = e.getMessage();
            System.out.print("===\nerror:" + e.getMessage() + "\n==");
        }

        return bufferedReader;
    }
    public String getMessage() {
        return message;
    }

    //other methods
    public void close() {

        //flush & close data output stream
        try {

            if (dataOutputStream != null) {

                dataOutputStream.flush();
                dataOutputStream.close();
            }
            if (bufferedReader != null) {

                bufferedReader.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    //other methods
    //get connection
    public HttpURLConnection getConn() {
        return conn;
    }
    //get input stream
    public InputStream getConnInputStream() {

        try {
            return conn.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    //get status code & error message
    public String getConnStatusCodeAndErrorMessage() {

        try {
            return Integer.toString( conn.getResponseCode()) + "\n" + conn.getErrorStream();
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    //get status code
    public String getConnStatusCode() {

        try {
            return Integer.toString(conn.getResponseCode());
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
