package POS_Libs.HttpConnection;

import android.util.Log;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * This class is to post to server and get response from server
 * Created by potingchiang on 2016-06-29.
 */
public class TalkToServer {

    //http connection
    private BasicHttpConnection_Json connection_json;
    //url string
    private String url;
    //request method
    private String request_Method;
    //data output stream
    private DataOutputStream dataOutputStream;
    //buffer reader
    private BufferedReader bufferedReader;
    //status code
    private String connStatusCode;

    //constructor
    public TalkToServer(BasicHttpConnection_Json connection_json, String url, String request_Method) {
        this.connection_json = connection_json;
        this.url = url;
        this.request_Method = request_Method;
    }

    //post to server
    public String doPostAndGetResponse(JSONObject order_Json) {

        //init. response result
        String result = "";
        //init json string
        String order_Json_Str = order_Json.toString();
        try {

            //init. connection
            connection_json = new BasicHttpConnection_Json(url, request_Method);
//            connection_json.getConn().setDoOutput(false);
            //init data output stream
            dataOutputStream = connection_json.getDataOutputStream();
            //write data into data output stream
            dataOutputStream.writeBytes(order_Json_Str);
            System.out.println("\n====\nData Json: \n" + order_Json_Str + "\n");

            //status code
            //setup status code
            connStatusCode = connection_json.getConnStatusCode();
//            Log.i("===\n" + "Status Code: \n", connection_json.getConnStatusCodeAndErrorMessage() + "\n===");
            System.out.println("===\n" + "Status code: \n" + connection_json.getConnStatusCodeAndErrorMessage() + "\n===");
            //get response from server in the input stream
            String line;
            bufferedReader = connection_json.getBufferedReader();
//            if (bufferedReader != null) {
            if (connection_json.getConnInputStream() != null) {

                while ((line = bufferedReader.readLine()) != null) {

                    result += line +"\n";
                }
            }
            else {

                result = "-1";
            }

        } catch (IOException e) {

            result = e.getMessage() ;
        }

        //clear, close stream and buffer reader
        connection_json.close();

        return result;
    }

    //get status code
    public String getConnStatusCode() {

        return connStatusCode;
    }
}
