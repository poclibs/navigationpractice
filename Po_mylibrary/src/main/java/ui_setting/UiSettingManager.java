package ui_setting;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.provider.Settings;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

/**
 * This class is to setup ui
 * Created by potingchiang on 2016-07-18.
 */
public class UiSettingManager {

    //basic variables
    private Context mContext;

    //constructor
    public UiSettingManager(Context mContext) {
        this.mContext = mContext;
    }

    //getter & setter

    //other methods
    //get custom fonts
    public Typeface getCustomTypeface(String ttf_Font) {

        //declare asset manager
        AssetManager mAssetManager = null;
        //check if context is null
        try {

            //init asset manager
            mAssetManager = mContext.getAssets();

        } catch (Exception e) {

            //error messages
            System.out.println("===\n" + "Context is null!" + "\n===");
            System.out.println("===\n" + e.getMessage() + "\n===");
        }

        //init font path
        String font_Path = "fonts/" + ttf_Font;

        //return result
        return Typeface.createFromAsset(mAssetManager, font_Path);
    }

    //decode byte array to bitmap
    public Bitmap getBitmapFromByteArray(byte[] mByteArray) {

        //decode byte array and return result
        return BitmapFactory.decodeByteArray(mByteArray, 0, mByteArray.length);
    }
    //decode resource, compress into it in byte array output stream
    public ByteArrayOutputStream getByteArrayOutputStreamFromResource(
            Resources resources, int resource_Id, Bitmap.CompressFormat format, int compress_Quality) {

        //init bitmap from resource
        Bitmap mBitmap = BitmapFactory.decodeResource(resources, resource_Id);
        //init byte array output stream
        ByteArrayOutputStream mOutputStream = new ByteArrayOutputStream();
        //compress bitmap into byte array output stream
        mBitmap.compress(format, compress_Quality, mOutputStream);

        //return result
        return mOutputStream;
    }
}
