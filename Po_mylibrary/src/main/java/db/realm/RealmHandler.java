package db.realm;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * This class is to handle realm db configuration and operation
 * Created by potingchiang on 2016-07-20.
 */
public abstract class RealmHandler {

    //declare variables
    //basic
    private Context mContext;
    private Realm mRealm;

    //constructor
    public RealmHandler(Context mContext) {
        this.mContext = mContext;

        //init realm configuration
        RealmConfiguration mConfig =
                new RealmConfiguration
                        .Builder(mContext)
                        .build();

        //clear up realm realm configuration
        Realm.deleteRealm(mConfig);

        //instantiate realm
        mRealm = Realm.getInstance(mConfig);

    }

    //getter & setter
    public Realm getmRealm() {
        return mRealm;
    }

    //db operation
    public abstract void insertData();

}
