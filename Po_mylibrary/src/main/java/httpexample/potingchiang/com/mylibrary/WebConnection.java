package httpexample.potingchiang.com.mylibrary;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * This class is to create connection to website with intent
 * Created by potingchiang on 2016-07-18.
 */
public class WebConnection {

    //variable statement
    //basic variable
    private Context mContext;
    private NetworkConnectionInfo mNetworkConnInfo;

    //constructor
    public WebConnection(Context mContext) {
        this.mContext = mContext;

        //init network connection info
        mNetworkConnInfo = new NetworkConnectionInfo(mContext);
    }

    //getter & setter

    //other methods
    public void openWebPage(String url) {

        //check if network is connected
        if (mNetworkConnInfo.isConnected()) {

            //make a connection to a web page
            //init. uri from url string
            Uri webPage = Uri.parse(url);
            //init intent to web page
            Intent web_Intent = new Intent(Intent.ACTION_VIEW, webPage);
            //check if the global package is null
            if (web_Intent.resolveActivity(mContext.getPackageManager()) != null) {

                //start intent
                mContext.startActivity(web_Intent);
            }
        }
        else {

            //error message
            Toast.makeText(mContext, "Network disconnected!", Toast.LENGTH_SHORT).show();
        }

    }
}
