package httpexample.potingchiang.com.mylibrary;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Suggestion: AsyncTask subclass better written in its used class as a inner class
 * This class is to get json data from server
 * Created by potingchiang on 2016-06-27.
 */

//note Void != void
public class GetJsonFromServer extends AsyncTask<String, Void, String> {

    //declaration
    //basic
    private Context context;
    //connection
    private HttpURLConnection conn = null;
    private BufferedReader in = null;
    //layout elements
    private String mJson;
    private TextView msg;
    //popup window
    private ProgressDialog dia_Loading;


    //constructor
    public GetJsonFromServer() {
    }
    public GetJsonFromServer(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {

        try {

            //get url object
            URL mUrl = new URL(params[0]);
            //get http connection
            conn = (HttpURLConnection) mUrl.openConnection();
            //setup connection configuration
            conn.setDoInput(true);
            //init. buffer reader
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            //get data from input stream
            //init. string builder
            StringBuilder sb = new StringBuilder();
            while ((mJson = in.readLine()) != null) {

                //append jSon data
                sb.append(mJson + "\n");
            }

            //clear & close connection
            in.close();
            //return result
            return sb.toString().trim();

        } catch (IOException e) {
            //error message
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            //return result
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
//        super.onPreExecute();

        //show progress dialog
        dia_Loading = ProgressDialog.show(context, "loading...", "Please wait...", true, true);
    }

    @Override
    protected void onPostExecute(String s) {
//        super.onPostExecute(s);

        try {

            //dismiss progress dialog
            dia_Loading.dismiss();
            //update ui
            msg.setText(s);
            //update mJson String
            mJson = s;

        } catch (Exception e) {

            msg.setText(e.getMessage());
        }
    }

    //getter & setter
    public String getmJson() {
        return mJson;
    }
    public void setmJson(String mJson) {
        this.mJson = mJson;
    }
    public TextView getMsg() {
        return msg;
    }
    public void setMsg(TextView msg) {
        this.msg = msg;
    }
}
