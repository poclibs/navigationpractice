package realm_db_handler;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

import Objects.SocialMedia_Sqlite;
import Objects.SocialMedia_realm;
import db.realm.RealmHandler;
import httpexample.potingchiang.com.mylibrary.WebConnection;
import httpexample.potingchiang.com.navigationpractice.R;
import io.realm.Realm;
import ui_setting.UiSettingManager;

/**
 * This class is to handle social media db operation in realm
 * Created by potingchiang on 2016-07-20.
 */
public class SocialMediaHandler extends RealmHandler {

    //declare variables
    private Context mContext;
    //ui setting manager
    private UiSettingManager mUiSettingManager;

    //constructor
    public SocialMediaHandler(Context mContext, UiSettingManager mUiSettingManager) {
        super(mContext);
        this.mContext = mContext;
        this.mUiSettingManager = mUiSettingManager;
    }

    //override methods
    //setup realm db for data insert
    @Override
    public void insertData() {

        //init realm
        Realm mRealm = super.getmRealm();
        //insert data
        if (mRealm.where(SocialMedia_realm.class).findAll().size() == 0) {

            //fb
            insertSocialMediaObj(
                    mRealm, "Facebook", "https://www.facebook.com", R.drawable.fb_img, "#A05391E7");
            //youtube
            insertSocialMediaObj(
                    mRealm, "youtube", "https://www.youtube.com", R.drawable.youtube_img, "#B5B62154");
            //twitter
            insertSocialMediaObj(
                    mRealm, "Twitter", "https://twitter.com", R.drawable.twitter_img, "#BF6ADEDE");
            //google
            insertSocialMediaObj(
                    mRealm, "Google", "https://www.google.ca", R.drawable.google_img, "#CCD9C761");
        }

    }
    //setup social media object
    private void insertSocialMediaObj(
            final Realm mRealm, final String socialMedia_Name, final String socialMedai_Url,
            final int img_Id, final String bg_Color) {

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm mRealm) {

                //init realm db for data insert
                SocialMedia_realm mSocialMedia = mRealm.createObject(SocialMedia_realm.class);

                //setup attributes
                //setup id
                int nextId = Integer.parseInt(mRealm.where(SocialMedia_realm.class).max("id").toString()) + 1;
                mSocialMedia.setId(nextId);
                //name
                mSocialMedia.setName(socialMedia_Name);
                //url
                mSocialMedia.setUrl(socialMedai_Url);
                //get byte array from resource
                ByteArrayOutputStream mOutputStream =
                        mUiSettingManager.getByteArrayOutputStreamFromResource(
                                mContext.getResources(),
                                img_Id,
                                Bitmap.CompressFormat.PNG,
                                100
                        );
                mSocialMedia.setImg_ByteArray(mOutputStream.toByteArray());
                //set bg_Color
                mSocialMedia.setBg_Color(bg_Color);

            }
        });
    }


}
