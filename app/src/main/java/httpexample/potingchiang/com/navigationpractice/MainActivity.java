package httpexample.potingchiang.com.navigationpractice;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import Objects.SocialMedia_realm;
import adapters.SocialMediaAdapter;
import httpexample.potingchiang.com.mylibrary.WebConnection;
import io.realm.Realm;
import realm_db_handler.SocialMediaHandler;
import ui_setting.UiSettingManager;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //declare variables
    //basic
    private WebConnection mWebConn;
    //ui setting manager
    private UiSettingManager mUiSettingManager;
    //social media db handler
    private SocialMediaHandler mSocialMediaHandelr;
//    //data set
//    private List<SocialMedia_Sqlite> mSocialMedia_Lst;
    //recycle view
    private RecyclerView mRecycleView;
    //recycle adapter
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init web connection
        mWebConn = new WebConnection(MainActivity.this);
        //init ui setting manager
        mUiSettingManager = new UiSettingManager(MainActivity.this);
        //init db handler
        mSocialMediaHandelr = new SocialMediaHandler(MainActivity.this, mUiSettingManager);
        //insert data into db by realm
        mSocialMediaHandelr.insertData();
        //setup data set
//        setDataSet();
        //init recycle view
        mRecycleView = (RecyclerView) findViewById(R.id.recycle_View);
        //setup fixed size
        assert mRecycleView != null;
        mRecycleView.setHasFixedSize(true);
        //setup layout manager
        mRecycleView.setLayoutManager(new LinearLayoutManager(this));
//        mRecycleView.setLayoutManager(new GridLayoutManager(this, 4));
        //init adapter
        mAdapter = new SocialMediaAdapter(getSocialMedia_RealmLst(), mUiSettingManager, mWebConn);
        //setup adapter
        mRecycleView.setAdapter(mAdapter);


        //tool bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //setup drawer
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

    }

    //setup realm data collection
    public List<SocialMedia_realm> getSocialMedia_RealmLst() {

        Realm mRealm = mSocialMediaHandelr.getmRealm();

//        return new ArrayList<>(mRealm.where(SocialMedia_realm.class).findAll());
        return mRealm.where(SocialMedia_realm.class).findAll();
    }

//    //setup data set
//    private void setDataSet() {
//
//        //init data set array list
//        mSocialMedia_Lst = new ArrayList<>();
//
//        //add data into data list array
//        //fb
//        mSocialMedia_Lst.add(setupSocialMediaObj("Facebook", "https://www.facebook.com", R.drawable.fb_icon));
//        //youtube
//        mSocialMedia_Lst.add(setupSocialMediaObj("youtube", "https://www.youtube.com", R.drawable.youtube_icon));
//        //twitter
//        mSocialMedia_Lst.add(setupSocialMediaObj("Twitter", "https://twitter.com", R.drawable.twitter_icon));
//        //google
//        mSocialMedia_Lst.add(setupSocialMediaObj("Google", "https://www.google.ca", R.drawable.google_img));
//
//    }
//    //setup social media object
//    private SocialMedia_Sqlite setupSocialMediaObj(String socialMedia_Name, String socialMedai_Url, int img_Id) {
//
//        //init social media object
//        SocialMedia_Sqlite mSocialMedia = new SocialMedia_Sqlite();
//        //setup attributes
//        //facebook
//        mSocialMedia.setName(socialMedia_Name);
//        mSocialMedia.setUrl(socialMedai_Url);
//        //get byte array from resource
//        ByteArrayOutputStream mOutputStream =
//                mUisettingManager.getByteArrayOutputStreamFromResource(
//                        getResources(),
//                        img_Id,
//                        Bitmap.CompressFormat.PNG,
//                        100
//                );
//        mSocialMedia.setImg_ByteArray(mOutputStream.toByteArray());
//
//        //return result
//        return mSocialMedia;
//    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_mainList) {

//            //init main intent
//            Intent main_Intent = new Intent(MainActivity.this, ScrollingActivity.class);
//            //start intent
//            startActivity(main_Intent);

        } else if (id == R.id.nav_fb) {

            //init. web url string
            String fb_Url = "https://www.facebook.com";

            //connect to web page
            mWebConn.openWebPage(fb_Url);


        } else if (id == R.id.nav_youtube) {

            //init. web url string
            String youtube_Url = "https://www.youtube.com";

            //connect to web page
            mWebConn.openWebPage(youtube_Url);

        } else if (id == R.id.nav_twitter) {

            //init. web url string
            String twitter_Url = "https://twitter.com";

            //connect to web page
            mWebConn.openWebPage(twitter_Url);

        } else if (id == R.id.nav_google) {

            //init. web url string
            String google_Url = "https://www.google.ca";

            //connect to web page
            mWebConn.openWebPage(google_Url);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //close realm
        mSocialMediaHandelr.getmRealm().close();
    }
}
