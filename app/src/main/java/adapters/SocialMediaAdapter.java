package adapters;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import Objects.SocialMedia_realm;
import httpexample.potingchiang.com.mylibrary.WebConnection;
import httpexample.potingchiang.com.navigationpractice.R;
import ui_setting.UiSettingManager;

/**
 * This class is for recycle view adapter
 * Created by potingchiang on 2016-07-19.
 */
public class SocialMediaAdapter extends RecyclerView.Adapter<SocialMediaAdapter.ViewHolder> {

    //declare variables
//    private List<SocialMedia_Sqlite> mSocialMedia_Lst;
    private List<SocialMedia_realm> mSocialMedia_Lst;
    //ui setting manager
    private UiSettingManager mUiSettingManager;
    //web connection
    private WebConnection webConn;

    //constructor
    public SocialMediaAdapter(
            List<SocialMedia_realm> mSocialMedia_Lst, UiSettingManager mUiSettingManager, WebConnection webConn) {
        this.mSocialMedia_Lst = mSocialMedia_Lst;
        this.mUiSettingManager = mUiSettingManager;
        this.webConn = webConn;

    }

    //view holder
    public static class ViewHolder extends RecyclerView.ViewHolder{

        //layout elements
        CardView my_cardview;
        ImageButton imgBtn_Link;
        TextView txt_Name;

        ViewHolder(View itemView) {
            super(itemView);

            //init layout elements
            my_cardview = (CardView) itemView;
//            my_cardview = (CardView) itemView.findViewById(R.id.my_cardview);
            imgBtn_Link = (ImageButton) itemView.findViewById(R.id.imgBtn_Link);
            txt_Name = (TextView) itemView.findViewById(R.id.txt_Name);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //inflate recycle view with item view
//        View mView = LayoutInflater.from(parent.getContext())
//                .inflate(
//                        R.layout.item_view_recycleview,
//                        parent,
//                        false
//                );
        // the the view is attached to parent root by default by using inflate
        // so set attachToRoot -> false
        // or will cause another one to attach to root and will cause memory wasting
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.item_view_cardview,
                        parent,
                        false
                );
        //assign view into view holder and return result
        return  new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //init social media object
        SocialMedia_realm mSocialMedia = mSocialMedia_Lst.get(position);

        //setup contents/layout elements
        holder.my_cardview.setBackgroundColor(Color.parseColor(mSocialMedia.getBg_Color()));
        holder.my_cardview.setPreventCornerOverlap(true);
        holder.my_cardview.setRadius(12);
        holder.imgBtn_Link.setImageBitmap(mUiSettingManager.getBitmapFromByteArray(mSocialMedia.getImg_ByteArray()));
        holder.txt_Name.setText(mSocialMedia.getName());
        holder.txt_Name.setTypeface(mUiSettingManager.getCustomTypeface("Shojumaru-Regular.ttf"));

        //init on click listener
        InitLayoutListener(holder, mSocialMedia.getUrl());

    }

    private void InitLayoutListener(ViewHolder holder, final String url) {

        holder.imgBtn_Link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //open connection to web page/uri data
                webConn.openWebPage(url);
            }
        });
    }

    @Override
    public int getItemCount() {

        return mSocialMedia_Lst.size();
    }


}
