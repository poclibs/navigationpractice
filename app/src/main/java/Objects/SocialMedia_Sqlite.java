package Objects;

import java.io.Serializable;

/**
 * This is the community objects for social media
 * Created by potingchiang on 2016-07-18.
 */
public class SocialMedia_Sqlite implements Serializable {

    //basic variables/attributes
    private String name;
    private String url;
    private byte[] img_ByteArray;

    //constructor
    public SocialMedia_Sqlite() {
    }

    //getter & setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getImg_ByteArray() {
        return img_ByteArray;
    }

    public void setImg_ByteArray(byte[] img_ByteArray) {
        this.img_ByteArray = img_ByteArray;
    }
}

