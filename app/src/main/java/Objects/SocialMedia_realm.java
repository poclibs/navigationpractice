package Objects;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * This class is practice by using realm
 * Created by potingchiang on 2016-07-20.
 */
public class SocialMedia_realm extends RealmObject{

    //basic variables/attributes
    @PrimaryKey
    private int id;
    private String name;
    private String url;
    private byte[] img_ByteArray;
    private String bg_Color;

    //constructor
    public SocialMedia_realm() {
    }

    //getter & setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getImg_ByteArray() {
        return img_ByteArray;
    }

    public void setImg_ByteArray(byte[] img_ByteArray) {
        this.img_ByteArray = img_ByteArray;
    }

    public String getBg_Color() {
        return bg_Color;
    }

    public void setBg_Color(String bg_Color) {
        this.bg_Color = bg_Color;
    }
}
